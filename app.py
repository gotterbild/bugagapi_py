import os
import socket
from flask import Flask
from flask_restplus import Api, Resource, fields
from models import db

# getting environment variables
flask_env = str(os.environ.get("FLASK_ENV"))
DB_PASS = str(os.environ.get("POSTGRES_PASSWORD"))

def create_app():
    app = Flask(__name__)
    app.debug = flask_env == 'development'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['SQLALCHEMY_DATABASE_URI'] = 'postgres://postgres:'+DB_PASS+'@postgres/postgres'
    # app.config.from_pyfile(config_filename)

    # db.app = app
    db.init_app(app)
    # db.create_all()
    from apis import api
    api.init_app(app)

    return app

if __name__ == "__main__":
    app = create_app()
    app.run(debug=True, host='0.0.0.0', port=80)
