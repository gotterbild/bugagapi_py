from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

db = SQLAlchemy()

class User(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(80), unique=True, nullable=False)
    pwd = db.Column(db.String(80), unique=False, nullable=False)
    date_created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    date_modified = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    recs = db.relationship('Rec', backref='author')
    bases = db.Column(db.ARRAY(item_type = db.Integer))

    def __init__(self, username, email):
        self.email = email
        self.pwd = pwd

    def __repr__(self):
        return '<User %r>' % self.username

class Base(db.Model):
    __tablename__ = 'base'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=False, nullable=False)
    color = db.Column(db.String(8), unique=False, nullable=False)
    date_modified = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    recs = db.relationship('Rec', backref='base')
    categories = db.relationship('Category', backref='base')
    users = db.Column(db.PickleType)

class Category(db.Model):
    __tablename__ = 'category'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=False, nullable=False)
    date_modified = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    color = db.Column(db.String(8), unique=False, nullable=False)
    icon = db.Column(db.String(20), unique=False, nullable=False)
    base = db.Column(db.Integer, db.ForeignKey('base.id'))

class Rec(db.Model):
    __tablename__ = 'rec'
    id = db.Column(db.Integer, primary_key=True)
    hash = db.Column(db.String(120), nullable=False)
    name = db.Column(db.String(120), nullable=False)
    price = db.Column(db.Float(precision=2), nullable=False)
    date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    date_modified = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    author = db.Column(db.Integer, db.ForeignKey('user.id'))
    base = db.Column(db.Integer, db.ForeignKey('base.id'))
    categories = db.Column(db.ARRAY(item_type = db.Integer))

# db.create_all()
