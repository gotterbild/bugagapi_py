from flask_restplus import Namespace, Resource

api = Namespace('rec', description='Records')

@api.route("/")
class Record(Resource):
    @api.doc(description='Returns list of Records')
    def get(self):
        return recs

    @api.doc(description='Create a new Record')
    def post(self):
        new_rec = api.payload
        new_rec["id"] = len(recs) + 1
        recs.append(new_rec)
        return new_rec, 201

@api.route("/<string:rec_id>")
class Record(Resource):
    @api.doc(description='Modify Record')
    def patch(self):
        new_rec = api.payload
        new_rec["id"] = len(recs) + 1
        recs.append(new_rec)
        return new_rec, 200