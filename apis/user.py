import os
from flask import jsonify, request, make_response
from flask_restplus import Namespace, Resource, fields
from functools import wraps
import jwt
import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from models import db

secret = str(os.environ.get("TOKEN_SECRET"))

api = Namespace('user', description='User')

def generate_token(email):
    tokendata = {
        'email' : email,
        # 'exp' : datetime.datetime.utcnow() + datetime.timedelta(weeks = 4)
    }
    token = jwt.encode(tokendata, secret)
    return token


user_model = api.model('User', {
    'email': fields.String,
    'pwd': fields.String
})

@api.route("/")
class User(Resource):
    @api.doc(description='Create a new User')
    # @api.marshal_with(user_model, as_list=True)
    @api.expect(user_model)
    def post(self):
        data = api.payload
        hashed_password = generate_password_hash(data['pwd'], method='sha256')
        new_user = User(
            email = data['email'],
            pwd = hashed_password
        )
        # return { 'data' : data , 'hashed_password' : hashed_password }, 201
        # db.session.add(new_user)
        # db.session.commit()
        token = generate_token(email = data['email'])
        # return jsonify({ 'token' : token }), 201
        return token, 201

@api.route("/<string:user_id>")
class User(Resource):
    @api.doc(description='Get User info')
    def get(self):
        data = api.payload
        hashed_password = generate_password_hash(data['pwd'], method='sha256')
        new_user = User(
            email = data['email'],
            pwd = hashed_password
        )
        token = generate_token(email = data['email'])
        return jsonify({ 'token' : token }), 201

@api.route("/login")
class User(Resource):
    @api.doc(description='Authenticate a user')
    def post(self):
        data = api.payload

        match = true
        if (match):
            token = generate_token(email = data['email'])
            return jsonify({ 'token' : token })
        else:
            return make_response('Access Denied', 401)

@api.route("/<string:user_id>")
class User(Resource):
    @api.doc(description='Modify user')
    def patch(self):
        new_user = api.payload
        return new_user, 201


