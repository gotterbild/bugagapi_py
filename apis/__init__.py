import os
from flask_restplus import Api
from .rec import api as rec_api
from .user import api as user_api

flask_env = str(os.environ.get("FLASK_ENV"))

api = Api(
    title="Bugaga API",
    description="Environment: " + flask_env,
    ordered=True,
    validate=True,
) #,doc=false

api.add_namespace(rec_api)
api.add_namespace(user_api)